#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, string, comment, blogutil, datetime, json, markdown
from bottle import route, run, request, redirect, template, static_file, error, TEMPLATES

@error(404)
def error404(error):
	return template('./views/404.tpl')

@route('/')
@route('/blog')
def blog():
	postlist = sorted([fold for fold in os.listdir("./posts")], reverse=True)
	return template('./views/blog.tpl', postlist = postlist)

@route('/blog/<post>')
def blog(post):
	if post not in os.listdir("./posts"):
		redirect('/404')
		
	header = open("./views/bloghead.tpl", "r").read()
	footer = open("./views/blogfoot.tpl", "r").read()
	text = markdown.markdown(open("./posts/"+post, "r").read(), extensions=['sane_lists'])
	com_path = "./comments/"+post+"_comments.txt"

	if not os.path.exists(com_path): # make sure comment file exists
	    file(com_path, 'w').close()
	    
	with open(com_path, "r") as com_file:
		comlist = [json.loads(line, object_hook=blogutil.json_to_object) for line in com_file]
	
	with open("./views/blogpost.tpl", "w") as f:
		f.write(header)
		f.write(text)
		f.write(footer)

	TEMPLATES.clear()  # clear the template cache - otherwise the posts will all appear the same

	return template('./views/blogpost.tpl', post_title = post, commentlist = comlist)

@route('/blog/<post>', method="POST")
def post_comment(post):
	today = datetime.datetime.utcnow().strftime("%b %d, %Y %H:%M:%S")
	name = request.forms.get("poster_name")
	reply_id = request.forms.get("reply_id")
	content = request.forms.get("content")
	com_path = "./comments/"+post+"_comments.txt"
	
	if not os.path.exists(com_path): # make sure comment file exists
	    file(com_path, 'w').close() 
	    
	with open(com_path, "r") as com_file:
		comlist = [json.loads(line, object_hook=blogutil.json_to_object) for line in com_file]

	if len(content) > 0 and content.isspace() == False:
		content = string.replace(content, "\n", "\t")
		com = comment.Comment(blogutil.file_len(com_path), reply_id, today, name, content)
		
		if com.reply_id != None:
			 com.reply_id = int(com.reply_id)

		if com.reply_id in [i.com_id for i in comlist]:
			for orig_com in comlist:
				if orig_com.com_id == com.reply_id:
					ci = comlist.index(orig_com)+1
			comlist.insert(ci, com)
		else:
			comlist.insert(0,com)
    
		with open(com_path, "w") as f:
			for i in comlist:
				line = json.dumps(i, default=blogutil.convert_to_json)
				f.write(line+'\n')

	redirect('/blog/'+post)

@route('/reply/<post_path>')
def reply(post_path):
	post, post_id = post_path.split('_')
	
	with open("./comments/"+post+"_comments.txt", "r") as com_file:
		comlist = [json.loads(line, object_hook=blogutil.json_to_object) for line in com_file]

	for com in comlist:
		if com.com_id == int(post_id):
			return template('./views/blogreply.tpl', com = com, post_id = post_id, post_title = post)

	redirect('/404')

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8000))
    run(host='localhost', port=port)
