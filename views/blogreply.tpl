<html>
<meta HTTP-equiv="Content-Type" content="text/html;charset=UTF-8">
<head>
	<title>Blog</title>
		<link rel="shortcut icon" href="/static/favicon.png" />
    	<link rel="stylesheet" type="text/css" href="/static/links.css" />
	<style>
		a:link {color: #67c4db; text-decoration:none;}
		a:hover {color: #67c4db;
				 text-shadow: -1px 1px 8px #67c4db, 1px -1px 8px #67c4db
				}
		body {background: #000; text-align: center; color: #67c4db; font-family: Courier New;}
	</style>
</head>

<body>
<div id="masthead">
	<h1> blog </h1> 
	<p> | — <a href="/">home</a> — | </p>
</div>
<h2>web log</h2>
<div style="margin-left: 250; margin-right: 250; text-align: left;">
	<div id="{{com.com_id}}" style="background-color: #000010; border: 1px solid #67c4db; width: 600px; padding-left: 15; padding-right: 15; margin-top: 10">
	<span style="margin-top: 15; float:right; font-size:10">id: {{com.com_id}}</span>
	<h4>{{com.name}} wrote:</h4>
	% for par in com.content.split('\t'):
	% if len(par) > 0:
		% if par[0] == ">":
			<p style="color:green"> {{par}} </p>
		% else:
			<p> {{par}} </p>
		% end
	% end
	% end
	<hr noshade size="1" style="color:#67c4db">
	<p style="font-size: 10"><i>Posted on {{com.post_date}}</i></p>
</div>
<br>
<h3> Reply to {{com.name}}: </h3>
Name: <input value="Visitor" name="poster_name" form="comment" type="text" style="margin-bottom: 20px; font-family: Monospace; color:#67c4db; background-color:black; border: 1px solid #67c4db" /> <br>
Reply id: <input value = "{{com.com_id}}" name="reply_id" form="comment" type="text" style="margin-bottom: 20px; font-family: Monospace; color:#67c4db; background-color:black; border: 1px solid #67c4db" /> <br>
<textarea name="content" form="comment" type="text" rows="7" cols="60" style="margin-right: 100;color:#67c4db; background-color:black; border: 1px solid #67c4db; resize:none"></textarea>
<form id = "comment" action = "/blog/{{post_title}}" method="post">
	<input value="Post Comment" type="submit" style="margin-top: 20; font-family: Courier; font-weight: bold; border: 1px solid #67c4db; background-color:black; color:#67c4db" />
</form>
</div>
</body>
</html>
