<html>
<meta HTTP-equiv="Content-Type" content="text/html;charset=UTF-8">
<head>
	<title>Blog</title>
		<link rel="shortcut icon" href="/static/favicon.png" />
    	<link rel="stylesheet" type="text/css" href="/static/links.css" />
	<style>
		a:link {color: #67c4db; text-decoration:none;}
		a:hover {color: #67c4db;
				 text-shadow: -1px 1px 8px #67c4db, 1px -1px 8px #67c4db
				}
		body {background: #000; text-align: center; color: #67c4db; font-family: Courier New;}
	</style>
</head>

<body>
<div id="masthead">
	<h1> blog </h1> 
	<p> | — <a href="/">home</a> — | </p>
</div>
<div style="margin-left: 250; margin-right: 250; text-align: left;">
% for post in postlist:
	<p><u><a href="/blog/{{post}}">{{post}}</a></u></p>
% end
</div>
</body>
</html>
