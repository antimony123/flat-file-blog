<html>
<meta HTTP-equiv="Content-Type" content="text/html;charset=UTF-8">
<head>
	<title>Not Found</title>
		<link rel="shortcut icon" href="/static/favicon.png" />
    	<link rel="stylesheet" type="text/css" href="/static/links.css" />
	<style>
		a:link {color: #db5c1f; text-decoration:none;}
		a:hover {color: #db5c1f;
				 text-shadow: -1px 1px 8px ##db5c1c, 1px -1px 8px ##db5c1f
				}
		body {background: #fff; text-align: justify; font-family: Courier New; font-size: 16;}
	</style>
</head>

<body style="margin-left: 350; margin-right: 350">
<p style="margin-top: 50; font-size: 150; text-align: center; color:#db5c1f"> 404 </p>
<p style="text-align:right; margin-top: 75; margin-right: 50"> <a href="/">home</a></p>
