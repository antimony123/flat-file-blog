<html>
<meta HTTP-equiv="Content-Type" content="text/html;charset=UTF-8">
<head>
	<title>Blog</title>
		<link rel="shortcut icon" href="/static/favicon.png" />
    	<link rel="stylesheet" type="text/css" href="/static/links.css" />
	<style>
		a:link {color: #67c4db; text-decoration:none;}
		a:hover {color: #67c4db;
				 text-shadow: -1px 1px 8px #67c4db, 1px -1px 8px #67c4db
				}
		body {background: #000; text-align: center; color: #67c4db; font-family: Courier New;}
	</style>
</head>

<body>
<div id="masthead">
	<h1> blog </h1> 
	<p> | — <a href="/">home</a> — | </p>
</div>
<div style="margin-left: 250; margin-right: 250; text-align: left;">
<h2>This is a Markdown example post.</h2>
<p>You may format your posts using Markdown:</p>
<h1>Header 1</h1>
<h2>Header 2</h2>
<h3>Header 3</h3>
<h3>etc.</h3>
<ol>
<li>ordered list 1</li>
<li>ordered list 2</li>
<li>ordered list 3</li>
</ol>
<ul>
<li>unordered list 1</li>
<li>unordered list 2</li>
<li>etc</li>
</ul>
<p><strong>bold</strong> <em>italic</em></p><br>
<hr noshade size="1" style="color:#67c4db">

<h3> Post a Comment: </h3>
Name: <input value="Visitor" name="poster_name" form="comment" type="text" style="margin-bottom: 20px; font-family: Monospace; color:#67c4db; background-color:black; border: 1px solid #67c4db" /> <br>
<textarea name="content" form="comment" type="text" rows="7" cols="60" style="margin-right: 100;color:#67c4db; background-color:black; border: 1px solid #67c4db; resize:none"></textarea>
<form id = "comment" method="post">
	<input value="Post Comment" type="submit" style="margin-top: 20; font-family: Courier; font-weight: bold; border: 1px solid #67c4db; background-color:black; color:#67c4db" />
</form>
<h3> Comments thus far: </h3>
% from reptest import replyDepth
% for com in commentlist:
	% if com.reply_id in [i.com_id for i in commentlist]:
		% repdepth, repname, repid = replyDepth(com, commentlist)
		% if repdepth < 6 and repdepth > 0:
			% divwidth = 600-repdepth*50
			% divmargin = repdepth*50
		% elif repdepth >= 6:
			% divwidth = 300
			% divmargin = 300
		% end
	<div id="{{com.com_id}}" style="background-color: #000010; border: 1px solid #67c4db; margin-left: {{divmargin}}; width: {{divwidth}}px; padding-left: 15; padding-right: 15; margin-top: 10; font-size: 12">
	<span style="margin-top: 15; float:right; font-size:10">id: {{com.com_id}}</span>
	<h4>{{com.name}} replied to {{repname}} ({{repid}}):</h4>
	% else:
	<div id="{{com.com_id}}" style="background-color: #000010; border: 1px solid #67c4db; width: 600px; padding-left: 15; padding-right: 15; margin-top: 10; font-size: 12">
	<span style="margin-top: 15; float:right; font-size:10">id: {{com.com_id}}</span>
	<h4>{{com.name}} wrote:</h4>
	% end
	% for par in com.content.split('\t'):
	% if len(par) > 0:
		% if par[0] == ">":
			<p style="color:green"> {{par}} </p>
		% else:
			<p> {{par}} </p>
		% end
	% end
	% end
	<hr noshade size="1" style="color:#67c4db">
	<span style="margin-bottom: 15; float:right; font-size:12"><a href="/reply/{{post_title}}_{{com.com_id}}">reply</a></span>
	<p style="font-size: 10"><i>Posted on {{com.post_date}}</i></p>
	</div>		
% end
</div>
</body>
</html>
