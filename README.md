# flat-file-blog

Flat file blog with comments and markdown support.
Currently only supports Linux.

## Requirements

- python 2.7
- Bottle framework 0.12.16
- Python Markdown 3.0.1

## Setup

Clone the repository: ```git clone https://antimony123@bitbucket.org/antimony123/flat-file-blog.git```

Within the cloned repository, you will see a posts directory and a comments directory.

The ```posts``` directory is for your blog posts. You place a file containing your post in this directory whenever you wish to create a new blog post.

The ```comments``` directory is for comments on your posts. Comment files appear as soon as a blog post is accessed. All comments for that post are stored in the corresponding comments file, in JSON format.

## Usage

#### Running the blog:

Change directory to the directory holding ```blog.py``` and run ```python blog.py```.

This will start up the server. The default address and port are set to ```localhost``` and ```8000```.

#### Creating a blog post:

Using your text editor of choice, write your content to a file. The content may be in Markdown or HTML. Once you are done, move the file to the ```posts``` directory.

#### Editing a blog post:

You may edit the file in the ```posts``` directory directly, or you may overwrite it by moving a file with the same name into the ```posts``` directory.

#### Comments:

Visitors will be able to post comments to any of your posts.

## Disclaimers

There is no CAPTCHA or spam filter implemented currently.
