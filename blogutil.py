def convert_to_json(obj):
	#print 'default(', repr(obj), ')'
	d = { '__class__':obj.__class__.__name__, 
		'__module__':obj.__module__,
		}
	d.update(obj.__dict__)
	return d

def json_to_object(d):
	if '__class__' in d:
		class_name = d.pop('__class__')
		module_name = d.pop('__module__')
		module = __import__(module_name)
		class_ = getattr(module, class_name)
		args = dict((key.encode('ascii'), value) for key, value in d.items())
		inst = class_(**args)
	else:
		inst = d
	return inst

def file_len(fname):
	try:
		with open(fname) as f:
			for i, _ in enumerate(f):
				pass
		return i + 1
	except:
		return 0
