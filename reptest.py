#!/usr/bin/env python
# -*- coding: utf-8 -*-

import comment, types

def replyDepth(com, clist):
	count = 0
	curr = com
	while type(curr.reply_id) == types.IntType:
		for i in clist:
			if curr.reply_id == i.com_id:
				count += 1
				curr = i
				if count == 1:
					name = curr.name
					rid = curr.com_id
	return count, name, rid
